const std = @import("std");

// BIMDAS babey
const EvaluationState = enum {
    braces,
    indices,
    mul_or_div,
    add_or_sub,
};

const Operator = enum {
    add, 
    sub,
    mul, 
    div, 
};

const Token = union(enum) {
    operator: Operator,
    constant: f32,
    open_brace,
    close_brace,
};

const EvaluationError = error {
    UnexpectedToken,
    UnexpectedResult,
};

const TokenizerError = error {
    UnknownToken,
    NotANumber,
};

const TokenizerResult = struct {
    token: Token,
    len: usize,
};

pub const Evaluator = struct {
    const Self = @This();
    const pool_size = 16 * 1024;

    fba: std.heap.FixedBufferAllocator,

    /// Creates a new evaluator
    /// Don't initialize in a loop, as this struct allocates a pool of memory
    pub fn init() !Self {
        
        var transient_memory = try std.heap.page_allocator.alloc(u8, pool_size);
        var fba = std.heap.FixedBufferAllocator.init(transient_memory);
        
        return .{
            .fba = fba,
        };
    }

    pub fn deinit(self: *Self) void {
        std.heap.page_allocator.free(self.fba.buffer);
    }

    pub fn run(self: *Self, slice: []const u8) !f32 {
        var tokens = try tokenizeExpression(slice, self.fba.allocator());
        return evaluate(&tokens);
    }
};

fn tokenizeExpression(slice: []const u8, allocator: std.mem.Allocator) !TokenList {
    var tokens = TokenList.init(allocator);
    var i: usize = 0;
    
    while (i < slice.len) {
        const c = slice[i];

        var result = try switch (c) {
            '0'...'9' => tokenizeNumber(slice[i..]),
            '+', '/', '-', '*' => tokenizeOperator(slice[i..]),
            '(', ')' => tokenizeBrace(slice[i..]),
            ' ', '\r', '\n', '\t' => {
                i += 1;
                continue;
            },
            else => TokenizerError.UnknownToken,
        };

        i += result.len;

        try tokens.append(result.token);
    }

    return tokens;
}

fn tokenizeNumber(slice: []const u8) !TokenizerResult {
    var i: usize = 0;
    for (slice) |c| {
        switch (c) {
            '0'...'9', '.' => i += 1,
            else => break,
        }
    }

    var num = std.fmt.parseFloat(f32, slice[0..i]) catch {
        return TokenizerError.NotANumber;
    };
    
    return .{
        .token = Token{.constant = num},
        .len = i,
    };
}

fn tokenizeBrace(slice: []const u8) !TokenizerResult {
    const c = slice[0];
    
    return switch (c) {
        '(' => .{.token = Token.open_brace, .len = 1},
        ')' => .{.token = Token.close_brace, .len = 1},
        else => unreachable
    };
}

fn tokenizeOperator(slice: []const u8) !TokenizerResult {
    const c = slice[0];

    // TODO: Make this able to parse operators that are larger than 1 character

    return switch (c) {
        '+' => .{.token = Token{.operator = .add}, .len = 1},
        '*' => .{.token = Token{.operator = .mul}, .len = 1},
        '/' => .{.token = Token{.operator = .div}, .len = 1},
        '-' => .{.token = Token{.operator = .sub}, .len = 1},
        else => unreachable,
    };
}

fn evaluate(tokens: *TokenList) !f32 {
    var iter = tokens.iterator() catch return 0.0;
    var state = EvaluationState.braces;

    while (true) {
        while (iter.next()) |node| {
            switch (node.token) {
                .operator => |op| {
                    switch (state) {
                        .mul_or_div => if (op != .mul and op != .div) continue,
                        .add_or_sub => if (op != .add and op != .sub) continue,
                        else => continue
                    }

                    var left_node = node.prev;
                    var right_node = node.next;

                    var left = left_node.?.token;
                    var right = right_node.?.token;

                    var sub_expr_result = try evaluateSubExpression(left, op, right);

                    try tokens.insertNewLink(left_node.?.prev, Token{.constant = sub_expr_result}, right_node.?.next);

                    // Skip the the token to the right of the sub expression.
                    iter.setCursor(right_node.?.next);
                },
                else => {}
            }
        }

        iter.reset();

        state = switch (state) {
            .braces => .indices,
            .indices => .mul_or_div,
            .mul_or_div => .add_or_sub,
            .add_or_sub => break,
        };
    }

    return switch (tokens.first.?.token) {
        .constant => |num| num,
        else => EvaluationError.UnexpectedResult
    };
}

fn evaluateSubExpression(left_token: Token, op: Operator, right_token: Token) !f32 {
    return switch (left_token) {
        .constant => |left| {
            return switch (right_token) {
                .constant => |right| {
                    return switch (op) {
                        .add => left + right,
                        .sub => left - right,
                        .mul => left * right,
                        .div => left / right,
                    };       
                },
                else => EvaluationError.UnexpectedToken,
            };
        },
        else => EvaluationError.UnexpectedToken,
    };
}

const TokenList = struct {
    const Self = @This();
    first: ?*Node,
    last: ?*Node,
    allocator: std.mem.Allocator,

    const Node = struct {
        token: Token,
        prev: ?*Node,
        next: ?*Node,
    };

    const Iterator = struct {
        cursor: ?*Node,
        parent_list: *TokenList,

        const Error = error {
            EmptyList
        };

        fn init(list: *TokenList) !Iterator {
            if (list.first == null) {
                return Error.EmptyList;
            }

            return .{
                .parent_list = list,
                .cursor = list.first,
            };
        }

        pub fn next(self: *@This()) ?*Node {
            if (self.cursor == null) {
                return null;
            }

            var result = self.cursor;
            self.cursor = self.cursor.?.next;
            return result;
        }

        pub fn setCursor(self: *@This(), node: ?*Node) void {
            self.cursor = node;
        }

        pub fn reset(self: *@This()) void {
            self.cursor = self.parent_list.first;
        }

    };

    pub fn init(allocator: std.mem.Allocator) TokenList {
        return .{
            .first = null,
            .last = null,
            .allocator = allocator
        };
    }

    pub fn append(self: *Self, token: Token) !void {
        if (self.first == null) {
            self.first = try self.allocator.create(Node);
            self.first.?.prev = null;
            self.first.?.next = null;
            self.first.?.token = token;
            self.last = self.first;
        } else {
            var next = self.last.?.next;
            next = try self.allocator.create(Node);

            // Set up bi-directional ptrs
            next.?.prev = self.last;
            self.last.?.next = next;
            
            next.?.next = null;
            next.?.token = token;
            self.last = next;
        }
    }

    /// Inserts a new Node that links a left and right nodes. 
    /// This *WILL* leak the nodes that were previously inbetween left and right
    pub fn insertNewLink(self: *Self, left: ?*Node, token: Token, right: ?*Node) !void {
        var new_node = try self.allocator.create(Node);
        new_node.token = token;
        new_node.prev = null;
        new_node.next = null;

        if (left != null) {
            new_node.prev = left;
            left.?.next = new_node;
        } else {
            self.first = new_node;
        }

        if (right != null) {
            new_node.next = right;
            right.?.prev = new_node;
        } else {
            self.last = new_node;
        }
    }

    pub fn iterator(self: *Self) !Iterator {
        return Iterator.init(self);
    }
};