const std = @import("std");
const builtin = @import("builtin");

const Evaluator = @import("evaluator.zig").Evaluator;

pub fn main() !void {
    const stdout = std.io.getStdOut().writer();
    const stdin = std.io.getStdIn().reader();

    var evaluator = try Evaluator.init();
    defer evaluator.deinit();

    var input_buffer: [1024]u8 = undefined;

    while (true) {
        try stdout.print("> ", .{});

        var bytes_read = try stdin.read(&input_buffer);
        const input = removeNewline(input_buffer[0..bytes_read]);

        if (std.mem.eql(u8, input, "exit")) {
            break;
        }

        var result = evaluator.run(input) catch |err| {
            try stdout.print("Error evaluating expression: {}\n", .{err});
            continue;
        };

        try stdout.print("{d}\n", .{result});
    }

    try stdout.print("Goodbye.\n", .{});
}

fn removeNewline(slice: []u8) []u8 {
    if (builtin.os.tag == std.Target.Os.Tag.windows and slice.len >= 2) {
        return slice[0..slice.len-2];
    } else if (slice.len >= 1) {
        return slice[0..slice.len-1];
    } else {
        return slice;
    }
}