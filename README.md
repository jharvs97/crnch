# crnch: minimal CLI number cruncher

## How to run
Clone this repo, get the [zig compiler](https://ziglang.org/download/) and then:
``` 
> cd where-you-cloned-the-repo
> zig build run
```